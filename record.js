var request = require('request');

var requestRecording = function(url, callback) {
  request(url, function(error, response, body) {
    if (!error) {
      console.log('Recording successfully requested');
      callback(true);
    } else {
      callback(false);
    }
  });
};

var requestLoop = function(callback) {
  request.post('http://euw.op.gg/summoner/ajax/spectator/', {
    form: {
      userName: player,
      force: true
    }
  }, function(error, httpResponse, body) {
    var findUrl = /\/summoner\/ajax\/requestRecording\.json\/gameId=\d+?"/;
    var recordingRequestUrl = body.match(findUrl);

    // stop if player is not ingame
    if (!recordingRequestUrl) {
      console.log(player + ' is not ingame (' + loopCounter + ')');
      callback(false);
      return;
    }

    // remove " at the end
    recordingRequestUrl = recordingRequestUrl.substring(0, recordingRequestUrl.length - 1);

    // apply domain
    var url = 'http://euw.op.gg' + recordingRequestUrl;

    // request recording
    requestRecording(url, callback);
  });
};

var loopCounter = 0;

// get player via console arguments
var player = process.argv[2] || 'hi im muff';

// start interval for checking
(function next() {
  loopCounter++;

  requestLoop(function(isIngame) {
    var delay = (isIngame) ? 15 * 60 * 1000 : 60 * 1000;
    setTimeout(next, delay);
  });
})();
